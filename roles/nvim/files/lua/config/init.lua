require("config.lazy")

require("config.autocmd")
require("config.keymaps")
require("config.opts")
require("config.quickfix")
require("config.taggregator")
