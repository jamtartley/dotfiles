#!/usr/bin/env zsh

setopt AUTO_CD
setopt AUTO_PARAM_SLASH
setopt AUTO_PUSHD
export FZF_DEFAULT_OPTS='--color=fg:#f8f8f2,bg:#282a36,hl:#bd93f9 --color=fg+:#f8f8f2,bg+:#44475a,hl+:#bd93f9 --color=info:#ffb86c,prompt:#50fa7b,pointer:#ff79c6 --color=marker:#ff79c6,spinner:#ffb86c,header:#6272a4'
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_DUPS
setopt HIST_VERIFY
setopt INC_APPEND_HISTORY
setopt LIST_PACKED
setopt MENU_COMPLETE
setopt NO_FLOW_CONTROL
setopt NO_HIST_IGNORE_ALL_DUPS
setopt NO_HIST_IGNORE_SPACE
setopt SHARE_HISTORY
