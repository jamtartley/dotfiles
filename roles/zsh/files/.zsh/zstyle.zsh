#!/usr/bin/env zsh

zstyle ':completion:*:*:git:*' script ~/.zsh/git-completion.zsh
zstyle ':completion:*:git-checkout:*' sort false
